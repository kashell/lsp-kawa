# IDE support for Kawa/Emacs using LSP

This provides IDE support for the Kawa family of languages
(primarily the Kawa dialect of Scheme), using the Language Server Protocol.

The is pre-alpha - not much works.
What does work:
- diagnostics display using flycheck
- goto definition (in current file)

There is as yet no support for "projects" or anything except single
stand-alone files.

## The server

The server is included in the Kawa git repositor (master branch),
but is not enabled by default.
It uses the [LSP4J](https://github.com/eclipse/lsp4j) interfaces.

### Prerequistites

You need 3 external jar files.  You can get 2 of them from the LSP4J project;
see the [LSP4J README.md](https://github.com/eclipse/lsp4j/blob/master/README.md)
for where you can get them:
- org.eclipse.lsp4j-_VERSION_.jar
- org.eclipse.lsp4j.jsonrpc-_VERSION_.jar

You also need [Google Gson](https://github.com/google/gson).
Doing `mvn install` will create the folowing in the `gson/target` subdirectory;
you can also download pre-built releases:
- gson-_VERSION_.jar

### Building the server

To build Kawa with LSP support, run configure with these flags:

    --with-lsp4j=/path/to/org.eclipse.lsp4j-VERSION.jar
    --with-lsp4j.jsonrpc=/path/to/org.eclipse.lsp4j.jsonrpc_VERSION_.jar
    --with-gson=/path/to/gson-_VERSION.jar

The `--with-lsp4j.jsonrpc` argument is optional if it is the same as
`--with-lsp4j` except with `org.eclipse.lsp4j` replaced
by `org.eclipse.lsp4j.jsonrpc`.

Otherwise [build Kawa as normal](https://www.gnu.org/software/kawa/Source-distribution.html).

## The Emacs client

The Emacs client is `lsp-kawa.el` in this directory.
It builds on `lsp-mode.el` and `lsp-ui.el`.

To add lsp-mode support for both Scheme and Java add the following to your `~/.emacs`:

    (add-to-list 'load-path "/path/to/lsp-java")
    (add-to-list 'load-path "/path/to/lsp-mode")
    (add-to-list 'load-path "/path/to/lsp-ui")
    (add-to-list 'load-path "/path/to/lsp-kawa")
    (require 'lsp-mode)
    (require 'lsp-java)
    (require 'lsp-kawa)
    (add-hook 'java-mode-hook #'lsp-java-enable)
    (add-hook 'scheme-mode-hook #'lsp-kawa-enable)
    (require 'lsp-ui)
    (add-hook 'lsp-mode-hook 'lsp-ui-mode)
    (add-hook 'java-mode-hook 'flycheck-mode)
    (add-hook 'scheme-mode-hook 'flycheck-mode)
    (setq lsp-kawa-command "/path/to/bin/kawa")

For Java support, you needs a Java LSP server, which you can  download from
[eclipse.jdt.ls release](https://github.com/eclipse/eclipse.jdt.ls/releases);
create a symblink `~/.emacs.d/eclipse.jdt.ls/server` that points to
the expanded download.

To add support for the Common Lisp (`.lisp` extension) and XQuery
languages, also add the following:

    (add-hook 'find-file-hook
              (lambda ()
                (let ((ext (file-name-extension buffer-file-name)))
                  (when (or (string= ext "lisp") (string= ext "xql"))
                    (lsp-kawa-enable) (flycheck-mode)))))

## Other (non-Emacs) clients

None yet.
