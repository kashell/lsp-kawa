;;; lsp-kawa.el --- Kawa support for lsp-mode

;; Version: 1.0
;; Package-Requires: ((emacs "25.1") (lsp-mode "3.0"))
;; Keywords: kawa
;; URL: https://gitlab.com/kashell/lsp-kawa

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(require 'scheme)
(require 'lsp-mode)

;;;###autoload
(defcustom lsp-kawa-server-install-dir (locate-user-emacs-file "eclipse.jdt.ls/server/")
  "Install directory for eclipse.jdt.ls-server.
The slash is expected at the end."
  :group 'lsp-mode
  :risky t
  :type 'directory )

(defun lsp-kawa--locate-server-jar ()
  "Return the jar file location of the language server.

The entry point of the language server is in `lsp-kawa-server-install-dir'/plugins/org.eclipse.equinox.launcher_`version'.jar."
  (ignore-errors
    (let* ((plugindir (expand-file-name "plugins" lsp-kawa-server-install-dir))
            (server-jar-filenames (directory-files plugindir t "org.eclipse.equinox.launcher_.*")))
      (if (not (= (length server-jar-filenames) 1))
        (message (format "Found more than one kawa language server entry points: %s" server-jar-filenames))
        (car server-jar-filenames)))))

(defun lsp-kawa--locate-server-config ()
  "returns the server config based on OS"
  (let ( (config (cond
                  ((string-equal system-type "windows-nt") ; Microsoft Windows
                   "config_win")
                  ((string-equal system-type "darwin") ; Mac OS X
                   "config_mac")
                  ((string-equal system-type "gnu/linux") ; linux
                   "config_linux"))))
    (message (format "using config for %s" config))
    (expand-file-name config lsp-kawa-server-install-dir)))

(defun lsp-kawa--ls-command ()
  (let ((server-jar (lsp-kawa--locate-server-jar))
        (server-config (lsp-kawa--locate-server-config))
        (root-dir (lsp-kawa--get-root)))
    `( "/home/bothner/Kawa/langserver/bin/kawa"
       "--langserver"
       "-configuration"
       ,server-config
       "-data"
       ,root-dir)))

(defun lsp-kawa--get-root ()
  "Retrieves the root directory of the kawa project root if available.

The current directory is assumed to be the kawa project’s root otherwise."
  (cond
   ((and (featurep 'projectile) (projectile-project-p)) (projectile-project-root))
   ((vc-backend default-directory) (expand-file-name (vc-root-dir)))
   (t (let ((project-types '("pom.xml" "build.gradle" ".project")))
	(or (seq-some (lambda (file) (locate-dominating-file default-directory file)) project-types)
	    default-directory)))))

(lsp-define-stdio-client lsp-kawa "kawa" #'lsp-kawa--get-root  (lsp-kawa--ls-command)
			 :ignore-regexps
			 '("^SLF4J: "
			   "^Listening for transport dt_socket at address: "))

(provide 'lsp-kawa)
;;; lsp-kawa.el ends here
